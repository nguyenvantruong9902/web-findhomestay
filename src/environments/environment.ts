// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  keySecret: 'secret12101999',
  socketUrl: 'http://localhost:3000',
  userUrl: 'http://localhost:3000/user',
  messageUrl: 'http://localhost:3000/message',
  fileDriveUrl: 'http://localhost:3000/file',
  postUrl:'http://localhost:3000/post',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
