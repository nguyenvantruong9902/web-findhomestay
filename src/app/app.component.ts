import { Component, OnInit } from '@angular/core';
import { SocketService } from './service/socket/socket.service';
import { DataShareService } from './service/utils/data-share/data-share.service';
import { UserService } from './service/user/user.service';
import { SetStorageService } from './service/utils/set-storage/set-storage.service';
import { Router } from '@angular/router';
import { EncodeDataService } from './service/utils/encode-data/encode-data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  getInfoUrl: any;

  constructor(
    private socketService: SocketService,
    private shareService: DataShareService,
    private userService: UserService,
    private storageService: SetStorageService,
    private encodeService: EncodeDataService,
    private router: Router
  ) { }

  ngOnInit() {
    if (location.href.includes('/google/info/')) {
      this.getInfoUrl = location.href.substring(location.href.indexOf('info/') + 5, location.href.length);
      let info = this.encodeService.decryptData(this.getInfoUrl);
      if (info != '' && info != undefined && info != null) {
        this.storageService.setLocalStorageUser(this.getInfoUrl);
        this.router.navigate(['']);
      }
    }
    this.connectSocket();
  }

  //Connect socket
  connectSocket() {
    if (this.shareService.getUserInfo() != '' && this.shareService.getUserInfo() != null && this.shareService.getUserInfo() != undefined) {
      let userId = this.shareService.getUserId();
      if (userId != '' && userId != undefined && userId != null) {
        this.socketService.connectSocket(userId);
      } else {
        this.storageService.deleteLocalStorageUser();
        this.storageService.deleteSessionUser();
        this.router.navigate(['']);
      }
    } else {
      this.router.navigate(['']);
    }
  }
}