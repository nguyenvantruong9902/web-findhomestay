import { Injectable } from '@angular/core';
import { Headers } from '@angular/http';
import { EncodeDataService } from '../encode-data/encode-data.service';

@Injectable({
  providedIn: 'root'
})
export class SetHeadersService {

  constructor(
    private encodeService: EncodeDataService,
  ) { }

  setUser(data) {
    let headers = new Headers();
    headers.append('x-un', data.username);
    data.password = this.encodeService.encryptData(data.password);
    headers.append('x-p', data.password);
    return headers;
  }
}
