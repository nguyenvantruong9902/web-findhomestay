import { TestBed } from '@angular/core/testing';

import { SetHeadersService } from './set-headers.service';

describe('SetHeadersService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SetHeadersService = TestBed.get(SetHeadersService);
    expect(service).toBeTruthy();
  });
});
