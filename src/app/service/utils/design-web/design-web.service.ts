import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DesignWebService {

  constructor() { }

  setSizeConversation() {
    setTimeout(() => {
      try {
        document.getElementById("scrollBottom").scrollTop = document.getElementById("scrollBottom").scrollHeight + 100;
      } catch (error) {

      }
    }, 1);
  }

  setSizeTextArea() {
    var text = document.querySelector('textarea');
    function resize() {
      if (text.scrollHeight <= 90) {
        text.style.height = 'auto';
        text.style.height = text.scrollHeight + 'px';
      } else {
        text.style.height = 90 + 'px';
        text.style.resize = 'none';
        text.style.overflowY = 'scroll';
      }
      if (text.scrollHeight < 90) {
        text.style.overflowY = 'hidden';
      }
    }
    function delayedResize() {
      window.setTimeout(resize, 0);
    }
    try {
      text.addEventListener('keydown', delayedResize);
      text.addEventListener('cut', delayedResize);
    } catch (error) {

    }
  }
}
