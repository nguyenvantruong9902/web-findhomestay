import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class CreateFormService {

  patternEmail: any = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  constructor() { }

  createMessageForm(): FormGroup {
    return new FormBuilder().group({
      message: [''],
      files: []
    });
  }

  createSendMailForm(): FormGroup {
    return new FormBuilder().group({
      email: ['', [Validators.required, Validators.pattern(this.patternEmail)]]
    });
  }

  createVerifyAccountForm() {
    return new FormBuilder().group({
      name: ['', Validators.required],
      verification: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(8)]]
    });
  }

  createLoginForm() {
    return new FormBuilder().group({
      email: ['', [Validators.required, Validators.pattern(this.patternEmail)]],
      password: ['', [Validators.required, Validators.minLength(8)]]
    });
  }

  createImageForm() {
    return new FormBuilder().group({
      image: ['', Validators.required],
      lengthImg: [0, [Validators.min(3)]]
    });
  }

  createContentForm() {
    return new FormBuilder().group({
      tite: ['', Validators.required],
      number: [0, Validators.required],
      street: [, Validators.required],
      ward: ['', Validators.required],
      county: ['', Validators.required],
      price: [0, Validators.required],
      connection: ['', Validators.required],
      description: ['', Validators.required]
    });
  }

  createPostEditForm() {
    return new FormBuilder().group({
      image: [''],
    });
  }
  createHomeForm() {
    return new FormBuilder().group({
      search: ['', Validators.required],
      location: ['', Validators.required],
      category: ['', Validators.required],
      price: ['', Validators.required],
      price1: ['', Validators.required],
    });
  }
  createPersonaleditForm(email: String, phone: String, name: String, birthday: Date) {
    return new FormBuilder().group({
      email: [email, [Validators.required, Validators.pattern(this.patternEmail)]],
      phone: [phone, Validators.required],
      image: ['', Validators.required],
      name: [name, Validators.required],
      birthday: [birthday, Validators.required],
    });
  }
}
