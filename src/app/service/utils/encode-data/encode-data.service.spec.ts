import { TestBed } from '@angular/core/testing';

import { EncodeDataService } from './encode-data.service';

describe('EncodeDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EncodeDataService = TestBed.get(EncodeDataService);
    expect(service).toBeTruthy();
  });
});
