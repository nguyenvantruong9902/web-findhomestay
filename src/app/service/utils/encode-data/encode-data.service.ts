import { Injectable } from '@angular/core';
import * as CryptoJS from "crypto-js";
import { environment } from "../../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class EncodeDataService {

  private keySecret: string = environment.keySecret;

  constructor(
  ) { }

  encryptData(data) {
    try {
      return CryptoJS.AES.encrypt(data, this.keySecret.trim()).toString();
    } catch (error) {

    }
  }

  decryptData(encryptData) {
    try {
      return CryptoJS.AES.decrypt(encryptData, this.keySecret.trim()).toString(CryptoJS.enc.Utf8);
    } catch (error) {

    }
  }

  // encryptObject(object) {
  //   return CryptoJS.AES.encrypt(JSON.stringify(object), this.keySecret.trim());
  // }

  // getBytesWords(encryptObject) {
  //   return CryptoJS.AES.decrypt(encryptObject, this.keySecret.trim());
  // }

  // decryptObject(bytes) {
  //   return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
  // }

  // wordArrayInit(user) {
  //   let object = {}
  //   var wordArray = this.getBytesWords(this.encryptObject(object));
  //   wordArray.words = [];
  //   wordArray.sigBytes = user.sigBytes;
  //   wordArray.words = user.words;
  //   return wordArray;
  // }
}
