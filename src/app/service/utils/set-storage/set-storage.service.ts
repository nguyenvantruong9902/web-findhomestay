import { Injectable } from '@angular/core';
import { User } from 'src/app/interfaces/user';
import { CookieService } from 'ngx-cookie-service';
import { EncodeDataService } from '../encode-data/encode-data.service';

@Injectable({
  providedIn: 'root'
})
export class SetStorageService {

  constructor(
    private cookieService: CookieService,
    private encodeService: EncodeDataService
  ) { }

  // Set localStorage
  setLocalStorageUser(user) {
    localStorage.setItem('_info', user);
  }

  //Delete localStorage
  deleteLocalStorageUser() {
    localStorage.removeItem('_info');
  }

  // Set cookies
  setSessionUser(user: User) {
    let toid = this.encodeService.encryptData(user._id);
    let toun = this.encodeService.encryptData(user.name);
    sessionStorage.setItem('toid', toid);
    sessionStorage.setItem('toun', toun);
  }

  deleteSessionUser() {
    sessionStorage.removeItem('toid');
    sessionStorage.removeItem('toun');
  }

  setCookiesSendMail() {
    let sm = this.encodeService.encryptData('sendmail');
    this.cookieService.set('sm', sm);
  }

  deleteCookiesSendMail() {
    this.cookieService.delete('sm');
  }
}
