import { TestBed } from '@angular/core/testing';

import { SetStorageService } from './set-storage.service';

describe('SetStorageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SetStorageService = TestBed.get(SetStorageService);
    expect(service).toBeTruthy();
  });
});
