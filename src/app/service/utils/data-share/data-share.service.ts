import { Injectable } from '@angular/core';
import { User } from 'src/app/interfaces/user';
import { BehaviorSubject, Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { EncodeDataService } from '../encode-data/encode-data.service';

@Injectable({
  providedIn: 'root'
})
export class DataShareService {

  private dataJson: any;
  private info: any;
  private user = new BehaviorSubject(null);
  selectedUser: Observable<User> = this.user.asObservable();

  constructor(
    private cookieService: CookieService,
    private encodeService: EncodeDataService
  ) {

  }

  getUserInfo(): any {
    let getLocalStorage = localStorage.getItem('_info');
    if (getLocalStorage != null && getLocalStorage != '' && getLocalStorage != undefined) {
      this.info = this.encodeService.decryptData(getLocalStorage);
      if (this.info) {
        this.info = JSON.parse(this.info);
      }
    }
    return this.info;
  }

  getUserId(): string {
    if (this.getUserInfo() != undefined && this.getUserInfo() != null && this.getUserInfo() != '') {
      return this.encodeService.encryptData(this.getUserInfo().id);
    }
  }

  getUserName(): string {
    if (this.getUserInfo() != undefined && this.getUserInfo() != null && this.getUserInfo() != '') {
      return this.getUserInfo().name;
    }
  }

  getToUserId(): string {
    return sessionStorage.getItem('toid');
  }
  getRole(): boolean {
    return true;
  }

  getToUsername(): string {
    let toun = sessionStorage.getItem('toun');
    return this.encodeService.decryptData(toun);
  }

  getDataFromJson(res: Object) {
    this.dataJson = res;
    return this.dataJson.data;
  }

  changeSelectedUser(message: User) {
    this.user.next(message);
  }

  removeLocalStorage() {
    return new Promise((resolve, reject) => {
      try {
        localStorage.removeItem('id');
        localStorage.removeItem('un');
        resolve(true);
      } catch (error) {
        resolve(false)
      }
    })
  }

  getCookieSenMail() {
    return this.cookieService.get('sm');
  }
}
