import { Injectable } from '@angular/core';
import { environment } from "../../../environments/environment";
import * as io from 'socket.io-client';
import { Observable } from 'rxjs';
import { Message } from 'src/app/interfaces/message';
import { EncodeDataService } from '../utils/encode-data/encode-data.service';

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  private uriSocket = environment.socketUrl;
  private socket: any;

  constructor(
    private encodeService: EncodeDataService,
  ) { }

  //Connect socket
  connectSocket(userId: string) {
    if (userId != null) {
      this.socket = io(this.uriSocket, { query: `id=${userId}` });
    }
  }

  //Get user following online
  getChatList(userId) {
    if (userId != null) {
      this.socket.emit('chat-list', { id: userId });
    }
    return new Observable(observer => {
      this.socket.on('chat-list-response', (data) => {
        try {
          if (data) {
            if (data.chatList) {
              data.chatList = this.decrypt(data.chatList);
            }
            if (data.userId) {
              data.userId = this.encodeService.decryptData(data.userId);
            }
            observer.next(data);
          }
        } catch (error) {
        }
      });
      return () => {
        this.socket.disconnect();
      };
    });
  }

  //Send message
  sendMessage(message: Message): void {
    this.socket.emit('add-message', this.encodeService.encryptData(JSON.stringify(message)));
  }

  //Receive message
  receiveMessages(): Observable<Message> {
    return new Observable(observer => {
      this.socket.on('add-message-response', (data) => {
        data = JSON.parse(this.encodeService.decryptData(data));
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
  }

  //Add user offline not following
  addUserOffline(data) {
    this.socket.emit('add-user-offline', data);
  }

  //Check user following or not
  checkIsFollowing() {
    return new Observable(observer => {
      this.socket.on('add-user-offline-response', (data) => {
        console.log(data);
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
  }

  //Typing message
  typingMessage(data) {
    this.socket.emit('typing', data);
  }

  //Receive typing message
  typingMessageResponse() {
    return new Observable(observer => {
      this.socket.on('typing-response', (data) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
  }

  //Add last message has been watched
  lastMessageWatched(data) {
    this.socket.emit('last-message', data);
  }

  //Receice last message has been watched
  lastMessageWatchedResponse() {
    return new Observable(observer => {
      this.socket.on('last-message-response', (data) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
  }

  //Logout
  logout(userId) {
    this.socket.emit('logout', { userId: userId });
    return new Observable(observer => {
      this.socket.on('logout-response', (data) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
  }

  private decrypt(data) {
    return JSON.parse(this.encodeService.decryptData(data));
  }
}
