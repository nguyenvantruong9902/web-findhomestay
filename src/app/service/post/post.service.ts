import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Http, Headers } from '@angular/http';
import { ToastrService } from 'ngx-toastr';
import { environment } from "../../../environments/environment";
import { Post } from "src/app/interfaces/post";
import { EncodeDataService } from '../utils/encode-data/encode-data.service';

export interface Response {
  error: any,
  success: any
}

@Injectable({
  providedIn: 'root'
})


export class PostService {
  private uri = environment.postUrl;
  private notification: string = 'Thông báo!';
  private Post: any;
  constructor(
    private toastr: ToastrService,
    private httpClient: HttpClient,
    private http: Http,
    private router: Router,
    private encodeService: EncodeDataService,
  ) { }

  AddPost(Post: Post):boolean {
    this.httpClient.post(`${this.uri}/addpost`, Post).subscribe((res: Response) => {
      if (res.error) {
        this.toastr.error(res.error, this.notification);
        return false;
      } else {
        this.toastr.success(res.success, this.notification);
        return true;
      }
    }, err => {
      console.log(err);
    })
    return true;
  }
  GetListPost(Approval) {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.uri}/listpost`, { Approval: Approval }).subscribe(res => {
        this.Post = res;
        var data = JSON.parse(this.Post._body).data;
        if (data != null) {
          var decrypt = this.encodeService.decryptData(data);
          resolve(JSON.parse(decrypt));
        } else {
          resolve(null);
        }
      }, err => reject(err));
    })
  }
  FindpostByID(ID) {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.uri}/getpostbyid`, { _id: ID }).subscribe(res => {
        this.Post = res;
        var data = JSON.parse(this.Post._body).data;
        if (data != null) {
          var decrypt = this.encodeService.decryptData(data);
          resolve(JSON.parse(decrypt));
        } else {
          resolve(null);
        }
      }, err => reject(err));
    })
  }
  getPostPersonal(IdUser, Approval) {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.uri}/getpostpersonal`, { IdUser: IdUser, Approval: Approval }).subscribe(res => {
        this.Post = res;
        var data = JSON.parse(this.Post._body).data;
        if (data != null) {
          var decrypt = this.encodeService.decryptData(data);
          resolve(JSON.parse(decrypt));
        } else {
          resolve(null);
        }
      }, err => reject(err));
    })
  }

}
