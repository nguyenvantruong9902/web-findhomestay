import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { saveAs } from "file-saver";

@Injectable({
  providedIn: 'root'
})
export class FileDriveService {

  private uri = environment.fileDriveUrl;
  private fileDownload: any;

  constructor(
    private http: HttpClient,
  ) { }

  uploadFileMessage(data) {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.uri}/upload`, data).subscribe((file: File) => {
        resolve(file);
      }, err => reject(err))
    })
  }

  downloadFileMessage(fileId, fileName) {
    let data = {
      fileId: fileId,
      fileName: fileName
    }
    this.http.post(`${this.uri}/download`, data, { responseType: 'blob' }).subscribe(res => {
      this.fileDownload = res;
      saveAs(this.fileDownload, data.fileName);
    }, err => alert('Đã xảy ra sự cố!'));
  }
  uploadImagePost(data) {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.uri}/uploadimagePost`, data).subscribe((file: File) => {
        resolve(file);
      }, err => reject(err))
    })
  }
  uploadImageUser(data) {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.uri}/uploadimageUser`, data).subscribe((file: File) => {
        resolve(file);
      }, err => reject(err))
    })
  }
}
