import { TestBed } from '@angular/core/testing';

import { FileDriveService } from './file-drive.service';

describe('FileDriveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FileDriveService = TestBed.get(FileDriveService);
    expect(service).toBeTruthy();
  });
});
