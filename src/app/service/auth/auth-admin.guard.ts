import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from
  '@angular/router';
import { Router } from '@angular/router';
import { DataShareService } from "../utils/data-share/data-share.service";
@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private myRoute: Router,
    private dataService: DataShareService) {
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) {
    let role = this.dataService.getRole()
    if (role) {
      return true;
    } else {
      this.myRoute.navigate(["index"]);
      return false;
    }
  }
}