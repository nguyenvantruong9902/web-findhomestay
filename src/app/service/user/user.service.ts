import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Http, Headers } from '@angular/http';
import { environment } from "../../../environments/environment";
import { SetStorageService } from '../utils/set-storage/set-storage.service';
import { DataShareService } from '../utils/data-share/data-share.service';
import { ToastrService } from 'ngx-toastr';
import { EncodeDataService } from '../utils/encode-data/encode-data.service';
import { SetHeadersService } from '../utils/set-headers/set-headers.service';
import { SocketService } from '../socket/socket.service';
import { Personal } from "../../interfaces/personal";

export interface Response {
  error: any,
  user: {
    id: any,
    info: any,
    success: any
  }
  success: any
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private uri = environment.userUrl;
  private user: any;
  private notification: string = 'Thông báo!';

  constructor(
    private toastr: ToastrService,
    private httpClient: HttpClient,
    private http: Http,
    private router: Router,
    private storageService: SetStorageService,
    private dataShareService: DataShareService,
    private encodeService: EncodeDataService,
    private socketService: SocketService
  ) {

  }

  // Login
  login(data) {
    data.password = this.encodeService.encryptData(data.password);
    return new Promise((resolve, reject) => {
      this.httpClient.post(`${this.uri}/login`, data).subscribe((res: Response) => {
        if (res.error) {
          this.toastr.error(res.error, this.notification);
          resolve('error');
        } else {
          this.storageService.setLocalStorageUser(res.user.info);
          this.toastr.success(res.user.success, this.notification);
          this.router.navigate(['']);
          setTimeout(() => {
            location.reload();
          }, 1500);
        }
      }, err => {
        reject(err);
        resolve('error');
      });
    });
  }

  loginGoogle() {
    return 'http://localhost:3000/auth/google';
  }

  sendMailVerify(email) {
    let sm = this.dataShareService.getCookieSenMail();
    if (this.encodeService.decryptData(sm) != 'sendmail') {
      this.storageService.setCookiesSendMail();
    }
    return new Promise((resolve, reject) => {
      this.httpClient.post(`${this.uri}/sendmail`, { email: email, sm: sm }).subscribe((res: Response) => {
        if (res.error) {
          this.toastr.error(res.error, this.notification);
          this.storageService.deleteCookiesSendMail();
          resolve('error');
        } else {
          resolve(this.encodeService.decryptData(res.user.success));
        }
      }, err => {
        reject(err);
        resolve('error');
      });
    })
  }

  register(data) {
    this.httpClient.post(`${this.uri}/register`, data).subscribe((res: Response) => {
      if (res.error) {
        this.toastr.error(res.error, this.notification);
      } else {
        this.toastr.success(res.user.success, this.notification);
      }
    }, err => {
      this.toastr.error('Lỗi truy cập!', this.notification);
    })
  }

  verificationUser(data) {
    return new Promise((resolve, reject) => {
      this.httpClient.post(`${this.uri}/verify`, data).subscribe((res: Response) => {
        if (res.error) {
          this.toastr.error(res.error, this.notification);
          resolve('error');
        } else {
          this.toastr.success(res.user.success, this.notification);
          var user = {
            id: res.user.id,
            name: data.name
          }
          this.storageService.setLocalStorageUser(user);
          this.storageService.deleteCookiesSendMail();
          this.router.navigate(['']);
          setTimeout(() => {
            location.reload();
          }, 2000);
        }
      }, err => {
        this.toastr.error('Lỗi truy cập!', this.notification);
        resolve('error');
        reject(err);
      })
    })
  }

  sendPhoneNumberVerify(data) {
    return new Promise((resolve, reject) => {
      this.httpClient.post(`${this.uri}/sendphone`, data).subscribe((res: Response) => {
        if (res.error) {
          this.toastr.error(res.error, this.notification);
          resolve('error');
        } else {
          this.toastr.success(res.user.success, this.notification);
          resolve(res.user.id);
        }
      }, err => {
        this.toastr.error('Lỗi truy cập!', this.notification);
        resolve('error');
        reject(err);
      })
    })
  }

  verifyPhoneNumber(data) {
    return new Promise((resolve, reject) => {
      this.httpClient.post(`${this.uri}/verifyphone`, data).subscribe((res: Response) => {
        if (res.error) {
          this.toastr.error(res.error, this.notification);
          resolve('error');
        } else {
          this.toastr.success(res.user.success, this.notification);
        }
      }, err => {
        this.toastr.error('Lỗi truy cập!', this.notification);
        resolve('error');
        reject(err);
      })
    })
  }

  getUserInfo(userId) {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.uri}/info`, { id: userId }).subscribe(res => {
        this.user = res;
        var data = JSON.parse(this.user._body).data;
        if (data != null) {
          var decrypt = this.encodeService.decryptData(data);
          resolve(JSON.parse(decrypt));
        } else {
          resolve(null);
        }

      }, err => reject(err));
    })
  }

  getAllUserFollowing(userId) {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.uri}/following`, { userId: userId }).subscribe(res => {
        this.user = res;
        var data = JSON.parse(this.user._body).data;
        if (data != null) {
          var decrypt = this.encodeService.decryptData(data);
          resolve(JSON.parse(decrypt));
        } else {
          resolve(null);
        }
      }, err => reject(err));
    })
  }

  logout() {
    this.socketService.logout(this.dataShareService.getUserId()).subscribe(res => {
      this.storageService.deleteLocalStorageUser();
      this.storageService.deleteSessionUser();
      this.router.navigate(['/']);
      setTimeout(() => {
        location.reload();
      }, 1000)
    });
  }

  UpdateUser(personal: Personal):boolean {
    this.httpClient.post(`${this.uri}/updateuser`, personal).subscribe((res: Response) => {
      if (res.error) {
        this.toastr.error(res.error, this.notification);
        return false;
      } else {
        this.toastr.success(res.success, this.notification);
        return true;
      }
    }, err => {
      console.log(err);
    })
    return true;
  }
}
