import { Injectable } from '@angular/core';
import { environment } from "../../../environments/environment";
import { HttpClient } from '@angular/common/http';
import { EncodeDataService } from '../utils/encode-data/encode-data.service';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  private uri = environment.messageUrl;
  private users: any;
  private messages: any;
  private success: any;

  constructor(
    private http: HttpClient,
    private encodeService: EncodeDataService
  ) { }

  getMesssages(fromUserId, toUserId) {
    let data = {
      fromUserId: fromUserId,
      toUserId: toUserId
    }
    return new Promise((resolve, reject) => {
      this.http.post(`${this.uri}/getMessages`, data).subscribe(res => {
        this.messages = res;
        resolve(JSON.parse(this.encodeService.decryptData(this.messages.data)));
      }, err => reject(err));
    })
  }

  getUsersMessage(userId) {
    return new Promise((resolve, reject) => {
      this.http.post(`${this.uri}/getUsers`, { userId: userId }).subscribe(res => {
        this.users = res;
        resolve(JSON.parse(this.encodeService.decryptData(this.users.data)));
      }, err => reject(err));
    })
  }

  updateIsViewMessage(fromUserId, toUserId) {
    let data = {
      fromUserId: fromUserId,
      toUserId: toUserId
    }
    this.http.post(`${this.uri}/updateIsView`, data).subscribe();
  }

  deleteMessages(fromUserId, toUserId) {
    let data = {
      fromUserId: fromUserId,
      toUserId: toUserId
    }
    return this.http.post(`${this.uri}/delete`, data);
  }
}
