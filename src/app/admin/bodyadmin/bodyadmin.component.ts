import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-bodyadmin',
  templateUrl: './bodyadmin.component.html',
  styleUrls: ['./bodyadmin.component.css']
})
export class BodyadminComponent implements OnInit {

  @Input() snav: any;
  
  constructor() { }

  ngOnInit() {
  }

}
