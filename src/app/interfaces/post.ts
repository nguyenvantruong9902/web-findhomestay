import { File } from './file';
export interface Post {
    IdUser: String,
    files: File[],
    Title:String,
    Contact:String,
    Description: String,
    Price: String,
    Address:String
}