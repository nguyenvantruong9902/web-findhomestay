import { File } from './file';
export interface Personal {
    _id: string,
    name: string,
    email: String,
    BirthDay: Date,
    phone: String,
    image: String,
}