import { User } from '../interfaces/user';

export interface ChatListResponse {
	chatList: User[];
	error: boolean;
	singleUser: boolean;
	userDisconnected: boolean;
	userId: string;
	userFollowTogether: User;
	timeOff: number;
}
