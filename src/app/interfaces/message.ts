import { File } from './file';

export interface Message {
    fromUserId: String,
    message: {
        text: String,
        files: File[]
    },
    toUserId: String,
    date: Date,
    time: String
}