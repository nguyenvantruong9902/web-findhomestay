export interface File {
    fileId: String,
    fileName: String,
    fileType: String
}