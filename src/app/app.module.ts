import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { HttpModule } from "@angular/http";
import {
  MatIconModule,
  MatInputModule,
  MatButtonModule,
  MatSelectModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatSidenavModule,
  MatListModule,
  MatCheckboxModule,
  MatCardModule,
  MatRadioModule,
  MatDatepickerModule,
  MatMenuModule,
  MatToolbarModule,
  MatTabsModule,
  MatSliderModule,
  MatNativeDateModule,
  MatStepperModule,
  MatBadgeModule,
  MatTooltipModule,
  MAT_HAMMER_OPTIONS,
  MatProgressSpinnerModule,
  MatProgressBarModule,
} from "@angular/material";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CookieService } from 'ngx-cookie-service';
import { ToastrModule } from 'ngx-toastr';
import { FileUploadModule } from "ng2-file-upload";
import { DatePipe } from "@angular/common";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './general/header/header.component';
import { FooterComponent } from './general/footer/footer.component';
import { BodyComponent } from './general/body/body.component';
import { ChatComponent } from './general/body/chat/chat.component';
import { ListComponent } from './general/body/chat/list/list.component';
import { ConversationComponent } from './general/body/chat/conversation/conversation.component';
import { HomeComponent } from './general/body/home/home.component';
import { FilterPipe } from "./filter.pipe";
import { AdminComponent } from './admin/admin.component';
import { MenuadminComponent } from './admin/menuadmin/menuadmin.component';
import { BodyadminComponent } from './admin/bodyadmin/bodyadmin.component';
import { DragdropDirective } from "../app/service/drag-drop/dragdrop.directive";
import { NotfoundComponent } from './general/body/notfound/notfound.component';
import { PostdetailComponent } from './general/body/post/postdetail/postdetail.component';
import { PersonalviewComponent } from './general/body/user/personalview/personalview.component';
import { PostshowComponent } from './general/body/post/postshow/postshow.component';
import { PersonaleditComponent } from './general/body/user/personaledit/personaledit.component';
import { AddpostComponent } from './general/body/post/addpost/addpost.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    BodyComponent,
    ChatComponent,
    ListComponent,
    ConversationComponent,
    HomeComponent,
    FilterPipe,
    AdminComponent,
    MenuadminComponent,
    BodyadminComponent,
    DragdropDirective,
    NotfoundComponent,
    PostdetailComponent,
    AddpostComponent,
    PersonalviewComponent,
    PostshowComponent,
    PersonaleditComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpModule,
    BrowserAnimationsModule,
    FileUploadModule,
    ToastrModule.forRoot({
      preventDuplicates: true,
      positionClass: 'toast-top-right',
      progressBar: true
    }),
    MatInputModule,
    MatListModule,
    MatSidenavModule,
    MatIconModule,
    MatButtonModule,
    MatSelectModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatCheckboxModule,
    MatCardModule,
    MatRadioModule,
    MatDatepickerModule,
    MatMenuModule,
    MatToolbarModule,
    MatTabsModule,
    MatSliderModule,
    MatNativeDateModule,
    MatStepperModule,
    MatBadgeModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    MatProgressBarModule
  ],
  providers: [
    CookieService,
    DatePipe,
    {
      provide: MAT_HAMMER_OPTIONS,
      useValue: { cssProps: { userSelect: true } },
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
