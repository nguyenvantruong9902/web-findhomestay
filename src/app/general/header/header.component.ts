import { Component, OnInit, Input } from '@angular/core';
import { CreateFormService } from 'src/app/service/utils/create-form/create-form.service';
import { FormGroup } from '@angular/forms';
import { UserService } from 'src/app/service/user/user.service';
import { EncodeDataService } from 'src/app/service/utils/encode-data/encode-data.service';
import { DataShareService } from 'src/app/service/utils/data-share/data-share.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  username: any;

  //Declare login form
  loginFormGroup: FormGroup;
  disableLogin: boolean = false;
  logged: boolean = false;

  //Declare register form
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  register: boolean = false;
  disableSenMail: boolean = false;
  disableRegister: boolean = false;

  constructor(
    private createFormService: CreateFormService,
    private userService: UserService,
    private encodeService: EncodeDataService,
    private shareService: DataShareService
  ) {
    this.createRegisterForm();
    this.createLoginForm();
  }

  ngOnInit() {
    setTimeout(() => {
      if (this.shareService.getUserInfo() != undefined && this.shareService.getUserInfo() != null && this.shareService.getUserInfo() != '') {
        this.logged = true;
        this.username = this.shareService.getUserName();
      }
    }, 1000);
  }

  async sendMailVerify() {
    this.disableSenMail = true;
    var passwordMail = await this.userService.sendMailVerify(this.getDataRegisterForm().email);
    if (passwordMail !== 'error') {
      var data = {
        sm: this.shareService.getCookieSenMail(),
        email: this.getDataRegisterForm().email,
        password: passwordMail
      }
      this.userService.register(data);
      this.register = true;
    } else {
      this.disableSenMail = false;
    }
  }

  async verification() {
    this.disableRegister = true;
    var error = await this.userService.verificationUser(this.getDataRegisterForm());
    if (error) {
      this.disableRegister = false;
    }
  }

  async login() {
    this.disableLogin = true;
    var error = await this.userService.login(this.getDataLoginForm());
    if (error) {
      this.disableLogin = false;
    }
  }

  loginGoogle() {
    return this.userService.loginGoogle();
  }

  async getUserInfo(userId) {
    var user = await this.userService.getUserInfo(userId);
  }

  logout() {
    this.userService.logout();
  }

  createLoginForm() {
    this.loginFormGroup = this.createFormService.createLoginForm();
  }

  createRegisterForm() {
    this.firstFormGroup = this.createFormService.createSendMailForm();
    this.secondFormGroup = this.createFormService.createVerifyAccountForm();
  }

  getDataRegisterForm() {
    let data = {
      email: this.firstFormGroup.controls['email'].value,
      verification: this.secondFormGroup.controls['verification'].value,
      name: this.secondFormGroup.controls['name'].value,
      password: this.secondFormGroup.controls['password'].value
    }
    return data;
  }

  getDataLoginForm() {
    let data = {
      email: this.loginFormGroup.controls['email'].value,
      password: this.loginFormGroup.controls['password'].value
    }
    return data;
  }

  //Animate
  mouseOverBell() {
    document.getElementById('bell').classList.add('animated', 'swing')
  }

  mouseOutBell() {
    document.getElementById('bell').classList.remove('animated', 'swing')
  }

  mouseOverChat() {
    document.getElementById('chat').classList.add('animated', 'wobble')
  }

  mouseOutChat() {
    document.getElementById('chat').classList.remove('animated', 'wobble')
  }

  mouseOverAccount() {
    document.getElementById('account').classList.add('animated', 'heartBeat')
  }

  mouseOutAccount() {
    document.getElementById('account').classList.remove('animated', 'heartBeat')
  }

  mouseOverPost() {
    document.getElementById('post').classList.add('animated', 'pulse')
  }

  mouseOutPost() {
    document.getElementById('post').classList.remove('animated', 'pulse')
  }
}
