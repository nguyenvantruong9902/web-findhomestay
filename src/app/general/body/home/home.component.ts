import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CreateFormService } from 'src/app/service/utils/create-form/create-form.service';
import { PostService } from "src/app/service/post/post.service";
import { UserService } from "src/app/service/user/user.service";
import { EncodeDataService } from 'src/app/service/utils/encode-data/encode-data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  type = [
    {
      "id": "cc",
      "name": "cc",
    },
    {
      "id": "cc",
      "name": "cc",
    },
    {
      "id": "cc",
      "name": "cc",
    },
  ];
  home: FormGroup;
  Post: any;
  user: any;
  constructor(
    private fb: FormBuilder,
    private createFormService: CreateFormService,
    private postservice: PostService,
    private userservice: UserService,
    private encode: EncodeDataService,
  ) { }

  ngOnInit() {
    this.home = this.createFormService.createHomeForm();
    this.getPost();
  }

  async getPost() {
    this.Post = await this.postservice.GetListPost(true);
  }
  getImage(id: any) {
    return "https://drive.google.com/uc?export=view&id=" + id;
  }
  // getImage2(Id: any) {
  //    this.getuser(Id);
  //   return "https://drive.google.com/uc?export=view&id=" + this.user.image;
  // }
  async getuser(Id: any) {
    this.user = await this.userservice.getUserInfo(this.encode.encryptData(Id));
  }
}
