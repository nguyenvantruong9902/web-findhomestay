import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CreateFormService } from 'src/app/service/utils/create-form/create-form.service';
import { ToastrService } from 'ngx-toastr';
import { FileDriveService } from 'src/app/service/file-drive/file-drive.service';
import { DataShareService } from 'src/app/service/utils/data-share/data-share.service';
import { EncodeDataService } from 'src/app/service/utils/encode-data/encode-data.service';
import { PostService } from "src/app/service/post/post.service";
import { File } from 'src/app/interfaces/file';

@Component({
  selector: 'app-addpost',
  templateUrl: './addpost.component.html',
  styleUrls: ['./addpost.component.css']
})
export class AddpostComponent implements OnInit {
  @ViewChild('inputfile', { static: true })
  inputfile: ElementRef;
  Content: FormGroup;
  Image: FormGroup;
  fileUrls = [];
  lenthImg: number;

  private ward: []
  private file: any;
  private fileErr: any[] = [];
  private files: File[] = [];
  index: any
  check: boolean = true;
  constructor(
    private toastr: ToastrService,
    private fb: FormBuilder,
    private fileDriveService: FileDriveService,
    private createFormService: CreateFormService,
    private datashare: DataShareService,
    private encode: EncodeDataService,
    private postservice: PostService,
  ) { }

  ngOnInit() {
    this.Image = this.createFormService.createImageForm();
    this.Content = this.createFormService.createContentForm();
  }
  onSelectFile(event) {
    let files = event.target.files;
    if (files) {
      for (let file of files) {
        let reader = new FileReader();
        if (file.type.includes('image/')) {
          reader.readAsDataURL(file);
        }
        reader.onload = (e: any) => {
          let data = {
            file: file,
            imgUrl: e.target.result,
          }
          this.fileUrls.push(data);
        }
      }
    }
    this.lenthImg = this.fileUrls.length;
    this.inputfile.nativeElement.value = null;
  }
  async addpost() {
    this.check = false
    await this.uploadFilesPost();
    this.check = this.postservice.AddPost({
      IdUser: this.encode.decryptData(this.datashare.getUserId()),
      files: this.files,
      Title: this.getDataContent().Tite,
      Contact: this.getDataContent().Contact,
      Description: this.getDataContent().Description,
      Price: this.getDataContent().Price,
      Address: this.getDataContent().number + " " + this.getDataContent().street + " " + this.getDataContent().ward + " " + this.getDataContent().county,
    });
    this.Image = this.createFormService.createImageForm();
    this.Content = this.createFormService.createContentForm();
  }
  deleteFile(i) {
    this.fileUrls.splice(i, 1);
    if (this.fileUrls.length === 0) {
      this.inputfile.nativeElement.value = null;
    }
  }
  async uploadFilesPost() {
    let lengthUploader = this.fileUrls.length;
    if (lengthUploader > 0) {
      for (let i = 0; i < lengthUploader; i++) {
        let formData = new FormData();
        formData.append('image', this.fileUrls[i].file);
        if (this.fileUrls[i].file.type == '') {
          this.fileErr.push(this.fileUrls[i].file.name);
          this.toastr.error(this.fileUrls[i].file.name, 'Không thể tải', {
            enableHtml: true
          });
        } else {
          this.file = await this.fileDriveService.uploadImagePost(formData);
          this.file.file.fileType = this.fileUrls[i].file.type;
          this.files.push(this.file.file);
        }
      }
    }
  }
  getDataContent() {
    let data = {
      Tite: this.Content.controls['tite'].value,
      Description: this.Content.controls['description'].value,
      Price: this.Content.controls['price'].value,
      number: this.Content.controls['number'].value,
      street: this.Content.controls['street'].value,
      ward: this.Content.controls['ward'].value,
      county: this.Content.controls['county'].value,
      Contact: this.Content.controls['connection'].value,
    }
    return data;
  }
}
