import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { PostService } from "src/app/service/post/post.service";
import { DataShareService } from 'src/app/service/utils/data-share/data-share.service';
import { EncodeDataService } from 'src/app/service/utils/encode-data/encode-data.service';

@Component({
  selector: 'app-postshow',
  templateUrl: './postshow.component.html',
  styleUrls: ['./postshow.component.css']
})
export class PostshowComponent implements OnInit {
  Post: any;
  user: any;
  Files:any;
  Title: any;
  IdUser: any;
  Description: any;
  Price: any;
  Address: any;
  Contact:any;
  constructor(
    private fb: FormBuilder,
    private postservice: PostService,
    private encode: EncodeDataService,
    private shareService: DataShareService
  ) { }

  ngOnInit() {
    this.getPost();
  }
  async getPost() {
    this.Post = await this.postservice.getPostPersonal(this.encode.decryptData(this.shareService.getUserId()), true);
    this.getposta(this.Post);
  }
  getposta(Post: any) {
    this.Title = this.Post.Title;
    this.Files = this.Post.files;
    this.IdUser = this.Post.IdUser;
    this.Description = this.Post.Description;
    this.Price = this.Post.Price;
    this.Address = this.Post.Address;
    this.Contact = this.Post.Contact;
  }
  getImage(id: any) {
    return "https://drive.google.com/uc?export=view&id=" + id;
  }
}
