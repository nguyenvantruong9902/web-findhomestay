import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { PostService } from "src/app/service/post/post.service";
import { EncodeDataService } from 'src/app/service/utils/encode-data/encode-data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from "src/app/service/user/user.service";

@Component({
  selector: 'app-postdetail',
  templateUrl: './postdetail.component.html',
  styleUrls: ['./postdetail.component.css']
})
export class PostdetailComponent implements OnInit {
  User: any;
  Phone: any=null;
  name: any=null;
  Post: any;
  Files: any
  Title: any;
  IdUser: any;
  Description: any;
  Price: any;
  Address: any;
  Contact: any;
  constructor(
    private fb: FormBuilder,
    private postservice: PostService,
    private encode: EncodeDataService,
    private router: ActivatedRoute,
    private userservice: UserService
  ) { }

  ngOnInit() {
    this.getPost();
  }
  async getPost() {
    let id = await this.router.snapshot.paramMap.get("ID")
    this.Post = await this.postservice.FindpostByID(id);
   await this.getpost(this.Post);
    this.getuser();

  }
  getImage(id: any) {
    return "https://drive.google.com/uc?export=view&id=" + id;
  }
  getpost(Post: any) {
    this.Title = this.Post.Title;
    this.Files = this.Post.files;
    this.IdUser = this.Post.IdUser;
    this.Description = this.Post.Description;
    this.Price = this.Post.Price;
    this.Address = this.Post.Address;
    this.Contact = this.Post.Contact;
  }
  async getuser() {
    this.User = await this.userservice.getUserInfo(this.encode.encryptData(this.IdUser));
    this.name = this.User.name;
    this.Phone = this.User.phone;
  }
}
