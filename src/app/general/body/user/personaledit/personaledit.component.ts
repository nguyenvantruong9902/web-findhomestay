import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CreateFormService } from 'src/app/service/utils/create-form/create-form.service';
import { UserService } from "src/app/service/user/user.service";
import { DataShareService } from 'src/app/service/utils/data-share/data-share.service';
import { EncodeDataService } from 'src/app/service/utils/encode-data/encode-data.service';
import { File } from 'src/app/interfaces/file';
import { ToastrService } from 'ngx-toastr';
import { FileDriveService } from 'src/app/service/file-drive/file-drive.service';
@Component({
  selector: 'app-personaledit',
  templateUrl: './personaledit.component.html',
  styleUrls: ['./personaledit.component.css']
})
export class PersonaleditComponent implements OnInit {
  urls: any;
  personaledit: FormGroup;
  personal: any
  name: String;
  fileUrls = [];
  private file: any;
  private fileErr: any[] = [];
  private files: File = null;
  check: boolean = true;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private createFormService: CreateFormService,
    private userservice: UserService,
    private encodeService: EncodeDataService,
    private shareService: DataShareService,
    private fileDriveService: FileDriveService,
    private toastr: ToastrService,
  ) { }

  ngOnInit() {
    this.getuser();
    this.personaledit = this.createFormService.createPersonaleditForm("", "", "", new Date);
  }
  async getuser() {
    this.personal = await this.userservice.getUserInfo(this.shareService.getUserId());
    this.name = this.personal.name;
    this.personaledit = this.createFormService.createPersonaleditForm(this.personal.email, this.personal.phone, this.personal.name, this.personal.BirthDay);
    this.urls = this.getImage(this.personal.image);
  }
  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        var reader = new FileReader();
        reader.onload = (event: any) => {
          this.urls = event.target.result;
        }
        reader.readAsDataURL(event.target.files[i]);
      }
    }
    this.SelectFileUpload(event);
  }

  SelectFileUpload(event) {
    let files = event.target.files;
    if (files) {
      this.fileUrls.splice(0, 1);
      for (let file of files) {
        let reader = new FileReader();
        if (file.type.includes('image/')) {
          reader.readAsDataURL(file);
        }
        reader.onload = (e: any) => {
          let data = {
            file: file,
            imgUrl: e.target.result,
          }
          this.fileUrls.push(data);
        }
      }
    }
  }
  async uploadImagesUser() {
    let lengthUploader = this.fileUrls.length;
    if (lengthUploader > 0) {
      for (let i = 0; i < lengthUploader; i++) {
        let formData = new FormData();
        formData.append('image', this.fileUrls[i].file);
        if (this.fileUrls[i].file.type == '') {
          this.fileErr.push(this.fileUrls[i].file.name);
          this.toastr.error(this.fileUrls[i].file.name, 'Không thể tải', {
            enableHtml: true
          });
        } else {
          this.file = await this.fileDriveService.uploadImageUser(formData);
          this.file.file.fileType = this.fileUrls[i].file.type;
          this.files = this.file.file;
        }
      }
    }
  }
  async Update() {
    this.check = false;
    await this.uploadImagesUser();
    this.check = this.userservice.UpdateUser(
      {
        _id: this.encodeService.decryptData(this.shareService.getUserId()),
        name: this.getData().name,
        email: this.getData().email,
        BirthDay: this.getData().birthday,
        phone: this.getData().phone,
        image: this.files == null ? this.personal.image : this.files.fileId,
      }
    );
    console.log(this.personal.image);
  }
  getData() {
    let data = {
      name: this.personaledit.controls['name'].value,
      email: this.personaledit.controls['email'].value,
      phone: this.personaledit.controls['phone'].value,
      birthday: this.personaledit.controls['birthday'].value,
    }
    return data;
  }
  getImage(id: any) {
    return "https://drive.google.com/uc?export=view&id=" + id;
  }
}
