import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/service/user/user.service';
import { DataShareService } from 'src/app/service/utils/data-share/data-share.service';
import { EncodeDataService } from 'src/app/service/utils/encode-data/encode-data.service';

@Component({
  selector: 'app-personalview',
  templateUrl: './personalview.component.html',
  styleUrls: ['./personalview.component.css']
})
export class PersonalviewComponent implements OnInit {
  personal: any;
  name:any;
  dateRegister:any;
  email:any;
  phone:any;
  image:any;
  constructor(
    private userService: UserService,
    private encodeService: EncodeDataService,
    private shareService: DataShareService
  ) { }

  ngOnInit() {
    this.getuser();
  }
  async getuser() {
    this.personal = await this.userService.getUserInfo(this.shareService.getUserId());
    this.name = this.personal.name;
    this.dateRegister = this.personal.dateRegister;
    this.email = this.personal.email;
    this.phone = this.personal.phone;
    this.image = this.personal.image;
  }
  getImage(id: any) {
    return "https://drive.google.com/uc?export=view&id=" + id;
  }
}
