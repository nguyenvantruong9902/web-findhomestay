import { Component, OnInit } from '@angular/core';
import { DataShareService } from 'src/app/service/utils/data-share/data-share.service';
import { SetStorageService } from 'src/app/service/utils/set-storage/set-storage.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  //Slidenav
  mode: string;
  fixedInViewport: boolean = false;
  screenWidth: number;

  searchText: string;


  constructor(
    private dataShareService: DataShareService,
    private storageService: SetStorageService
  ) { }

  ngOnInit() {
    this.responsiveChat();
    this.checkUserExist();
  }

  responsiveChat() {
    this.screenWidth = window.innerWidth;
    window.onresize = () => {
      this.screenWidth = window.innerWidth;
      this.checkUserExist();
    };
  }

  checkUserExist() {
    if (this.dataShareService.getUserId() == null) {
      this.storageService.deleteSessionUser();
    } else {
      this.viewListUser();
    }
  }

  viewListUser() {
    try {
      if (this.screenWidth > 700) {
        this.mode = "side";
        this.fixedInViewport = true;
        document.getElementById('content-phone').style.height = (screen.height - 360) + 'px';
      } else {
        this.mode = "over";
        document.getElementById('conversation').style.height = (screen.height - 250) + 'px';
        document.getElementById('content-phone').style.height = (screen.height - 320) + 'px';
      }
      document.getElementById("sidenav-content").style.height = window.innerHeight + 'px';
    } catch (error) {

    }
  }

}
