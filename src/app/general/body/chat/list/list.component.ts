import { Component, OnInit, Input } from '@angular/core';
import { DataShareService } from 'src/app/service/utils/data-share/data-share.service';
import { SocketService } from 'src/app/service/socket/socket.service';
import { ChatListResponse } from 'src/app/interfaces/chat-list';
import { User } from 'src/app/interfaces/user';
import { UserService } from 'src/app/service/user/user.service';
import { ToastrService } from 'ngx-toastr';
import { SetStorageService } from 'src/app/service/utils/set-storage/set-storage.service';
import { EncodeDataService } from 'src/app/service/utils/encode-data/encode-data.service';
import { MessageService } from 'src/app/service/message/message.service';

export interface UserNotFollow {
  isFollow: boolean,
  user: User
}

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})

export class ListComponent implements OnInit {

  private userId: any;
  allUserFollowing: any;

  //Online
  chatListUsersOnline: User[] = [];
  intervalOnline: any;
  //Offline
  chatListUsersOffline: User[] = [];
  intervalOffline: any;

  user: any;
  users: any;
  userOffline: any;
  message: any;
  userFollow: any;
  selectedUserId: any;


  @Input() searchText: string;
  loading: boolean = true;

  constructor(
    private toastr: ToastrService,
    private socketService: SocketService,
    private userService: UserService,
    private dataShare: DataShareService,
    private storageService: SetStorageService,
    private encodeService: EncodeDataService,
    private messageService: MessageService
  ) { }

  ngOnInit() {
    this.userId = this.dataShare.getUserId();
    this.getListUserOnline();
    this.getAllUserFollowing();
  }

  //Get chat list user online with socket
  getListUserOnline() {
    this.socketService.getChatList(this.userId).subscribe((chatListResponse: ChatListResponse) => {
      this.renderChatList(chatListResponse);
    });
  }

  //Realtime chat list
  renderChatList(chatListResponse: ChatListResponse) {
    if (!chatListResponse.error) {
      if (chatListResponse.singleUser) {
        setTimeout(async () => {
          clearInterval(this.intervalOnline);
          this.user = await this.userService.getUserInfo(this.userId);
          if (this.user !== undefined) {
            var userInChatList = this.user.following.find(element => {
              return element.userId == chatListResponse.userId;
            });
            var userExistOnline = this.uniqueInList(this.chatListUsersOnline, chatListResponse.userId);
            var userExistOffline = this.uniqueInList(this.chatListUsersOffline, chatListResponse.userId);
            if (userInChatList != undefined && userExistOnline < 0) {
              if (userExistOffline >= 0) {
                this.chatListUsersOffline.splice(userExistOffline, 1);
              }
              this.chatListUsersOnline = this.chatListUsersOnline.concat(chatListResponse.chatList);
            } else if (userExistOffline >= 0) {
              this.chatListUsersOffline[userExistOffline].isOnline = true;
            }
          }
        }, 3000)
      } else if (chatListResponse.userDisconnected) {
        var userId = this.encodeService.decryptData(chatListResponse.userId);
        this.intervalOnline = setInterval(async () => {
          if (new Date().getTime() - chatListResponse.timeOff >= 30000) {
            var loggedOutUserOnline = this.uniqueInList(this.chatListUsersOnline, userId);
            var loggedOutUserOffline = this.uniqueInList(this.chatListUsersOffline, userId);
            if (loggedOutUserOnline >= 0) {
              this.chatListUsersOnline.splice(loggedOutUserOnline, 1);
              this.message = await this.messageService.getMesssages(this.userId, chatListResponse.userId);
              if (this.message.length > 0) {
                this.userFollow = await this.userService.getUserInfo(chatListResponse.userId);
                this.chatListUsersOffline = this.chatListUsersOffline.concat(this.userFollow);
              }
            }
            if (loggedOutUserOffline >= 0) {
              this.chatListUsersOffline[loggedOutUserOffline].isOnline = false;
            }
            clearInterval(this.intervalOnline);
          }
        }, 1000);
      } else {
        this.chatListUsersOnline = chatListResponse.chatList;
        this.getUsersMessage(this.chatListUsersOnline);
      }
    } else {
      this.toastr.error('Không thể tải danh sách user online!')
    }
  }

  //Get user from message not exist in following
  async getUsersMessage(chatListUsersOnline) {
    this.users = await this.messageService.getUsersMessage(this.userId);
    var arrayOffline = this.users;
    var intersection = this.chatListUsersOnline.filter(online => arrayOffline.map(offline => offline._id).includes(online._id));
    intersection.forEach(element => {
      arrayOffline.splice(arrayOffline.indexOf(element), 1)
    });
    if (this.userOffline) {
      var userExist = this.uniqueInList(arrayOffline, this.userOffline._id);
      if (userExist < 0) {
        arrayOffline = arrayOffline.concat(this.userOffline);
      }
    }
    arrayOffline = this.uniqueInTwoList(chatListUsersOnline, arrayOffline);
    this.chatListUsersOffline = arrayOffline;
    this.socketService.checkIsFollowing().subscribe((data: UserNotFollow) => {
      if (!data.isFollow) {
        var userExist = this.uniqueInList(this.chatListUsersOffline, data.user._id);
        if (userExist < 0) {
          this.chatListUsersOffline = this.chatListUsersOffline.concat(data.user);
        }
      }
    })
  }

  //Get all user following
  getAllUserFollowing() {
    setInterval(async () => {
      setTimeout(async () => {
        this.allUserFollowing = await this.userService.getAllUserFollowing(this.userId);
        this.checkUniqueAll();
        this.progressSnipper();
      }, 1000);
      var toid = this.dataShare.getToUserId();
      if (toid) {
        this.user = await this.userService.getUserInfo(toid);
        this.userOffline = this.user;
        if (this.allUserFollowing != undefined) {
          var findInAllFollowing = this.allUserFollowing.findIndex(item => item._id == this.user._id);
          if (findInAllFollowing >= 0) {
            this.chatListUsersOffline = this.chatListUsersOffline.concat(this.user);
          }
        }
      }
    }, 2000)
  }

  //Select user
  selectedUser(user: User) {
    this.selectedUserId = user._id;
    this.storageService.setSessionUser(user);
    this.dataShare.changeSelectedUser(user);
    if (this.allUserFollowing) {
      var findInAllFollowing = this.allUserFollowing.findIndex(item => item._id == user._id);
      if (findInAllFollowing >= 0) {
        this.allUserFollowing = [];
        this.loading = true;
        this.userOffline = user;
        var userExistOffline = this.uniqueInList(this.chatListUsersOnline, this.userOffline._id);
        if (userExistOffline < 0) {
          this.chatListUsersOffline = this.chatListUsersOffline.concat(user);
        }
      }
    }
  }

  isUserSelected(userId: string): boolean {
    var toUserId = this.encodeService.decryptData(this.dataShare.getToUserId());
    if (!toUserId) {
      return false;
    }
    return toUserId == userId ? true : false;
  }

  uniqueInList(list, userId) {
    return list.findIndex(item => item._id == userId);
  }

  uniqueInTwoList(listNotRemote, listRemote) {
    listRemote = listRemote.filter(item1 => !listNotRemote.some(item2 => item2._id == item1._id));
    return listRemote;
  }

  checkUniqueAll() {
    this.allUserFollowing = this.uniqueInTwoList(this.chatListUsersOnline, this.allUserFollowing);
    this.allUserFollowing = this.uniqueInTwoList(this.chatListUsersOffline, this.allUserFollowing);
  }

  progressSnipper() {
    setTimeout(() => {
      this.loading = false;
    }, 100);
  }
}
