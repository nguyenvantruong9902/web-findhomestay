import { Component, OnInit, ViewChild, ElementRef, Directive } from '@angular/core';
import { DatePipe } from "@angular/common";
import { DataShareService } from 'src/app/service/utils/data-share/data-share.service';
import { DesignWebService } from 'src/app/service/utils/design-web/design-web.service';
import { User } from 'src/app/interfaces/user';
import { SocketService } from 'src/app/service/socket/socket.service';
import { Message } from 'src/app/interfaces/message';
import { MessageService } from 'src/app/service/message/message.service';
import { FormGroup } from '@angular/forms';
import { CreateFormService } from 'src/app/service/utils/create-form/create-form.service';
import { ToastrService } from 'ngx-toastr';
import { File } from 'src/app/interfaces/file';
import { FileDriveService } from 'src/app/service/file-drive/file-drive.service';
import { EncodeDataService } from 'src/app/service/utils/encode-data/encode-data.service';
import { SetStorageService } from 'src/app/service/utils/set-storage/set-storage.service';

export interface Typing {
  text: String,
  toUserId: String
}

@Component({
  selector: 'app-conversation',
  templateUrl: './conversation.component.html',
  styleUrls: ['./conversation.component.css']
})
export class ConversationComponent implements OnInit {
  //Set localstorage
  username: any;
  userId: any;
  encodeUserId: any;
  //Set cookies
  toUserId: any;
  toUsername: any;
  typing: any;
  watched: any;
  fileUrls = [];
  @ViewChild('fileInput', { static: true })
  fileInput: ElementRef;

  response: any;
  uploading: boolean = false;

  private messageForm: FormGroup;
  private messages: any;
  private file: any;
  private fileErr: any[] = [];
  private days: any[] = [];
  private dates: any[] = [];
  private daysOld: any[] = [];
  private files: File[] = [];

  constructor(
    private datePipe: DatePipe,
    private toastr: ToastrService,
    private dataShareService: DataShareService,
    private designWebService: DesignWebService,
    private socketService: SocketService,
    private messageService: MessageService,
    private createFormService: CreateFormService,
    private fileDriveService: FileDriveService,
    private encodeService: EncodeDataService,
    private storageService: SetStorageService
  ) {
    this.messageForm = this.createFormService.createMessageForm();
  }

  ngOnInit() {
    document.body.style.overflow = 'hidden';
    this.designWebService.setSizeTextArea();
    this.getDataShare();
    this.typingMessage();
    this.listenForMessages();
    this.typingResponse();
    this.lastMessageWatchedResponse();
    this.showMessageUser();
    if (this.toUserId !== '' && this.toUserId !== null && this.toUserId !== undefined && this.toUsername !== '' && this.toUsername !== null && this.toUsername !== undefined) {
      document.getElementById('conversation').style.display = 'block';
      this.socketService.lastMessageWatched({ fromUserId: this.userId, toUserId: this.toUserId });
      this.getMessages(this.toUserId);
      this.encodeUserId = this.encodeService.decryptData(this.userId);
    }
  }

  showMessageUser() {
    this.dataShareService.selectedUser.subscribe((selectedUser: User) => {
      if (selectedUser != null && selectedUser != undefined) {
        if (selectedUser._id == this.encodeService.decryptData(this.toUserId)) {
          return;
        }
        this.resetDates();
        document.getElementById('conversation').style.display = 'block';
        this.getDataShare();
        this.socketService.lastMessageWatched({ fromUserId: this.userId, toUserId: this.encodeService.encryptData(selectedUser._id) });
        this.encodeUserId = this.encodeService.decryptData(this.userId);
        this.messageService.updateIsViewMessage(this.encodeService.encryptData(selectedUser._id), this.userId);
        this.getMessages(this.encodeService.encryptData(selectedUser._id));
      }
    })
  }

  async getMessages(toUserId) {
    this.messages = await this.messageService.getMesssages(this.userId, toUserId);
    for (let i = 0; i < this.messages.length; i++) {
      var date = new Date(this.messages[i].date);
      this.days.push(this.datePipe.transform(date, "MM/dd/yyyy"));
    }
    this.getDatesFromMessage();
    this.toUsername = this.dataShareService.getToUsername();
    this.designWebService.setSizeConversation();
  }

  getDataShare() {
    this.userId = this.dataShareService.getUserId();
    this.username = this.dataShareService.getUserName();
    this.toUserId = this.dataShareService.getToUserId();
    this.toUsername = this.dataShareService.getToUsername();
  }

  listenForMessages() {
    this.socketService.receiveMessages().subscribe((socketResponse: Message) => {
      let userId = this.encodeService.decryptData(this.toUserId);
      if (userId != null && userId == socketResponse.fromUserId) {
        this.messages = [...this.messages, socketResponse];
        this.setDatesForMessage();
        this.designWebService.setSizeConversation();
      }
    });
  }

  submitMessageForm(event) {
    if (event.keyCode == 13) {
      this.sendMessage();
    }
  }

  async sendMessage() {
    var message = this.messageForm.controls['message'].value;
    var text = '';
    if (message != null) {
      text = message.trim();
    }
    var data = {
      text: '',
      toUserId: this.toUserId
    }
    this.socketService.typingMessage(data);
    if (message == '' && this.fileUrls.length == 0) {
      this.toastr.error('Vui lòng nhập tin nhắn!', 'Thông báo!');
    } else {
      await this.uploadFilesMessage();
      this.socketService.addUserOffline({ fromUserId: this.encodeService.decryptData(this.userId), toUserId: this.encodeService.decryptData(this.toUserId) });
      this.sendAndUpdateMessages({
        fromUserId: this.encodeService.decryptData(this.userId),
        message: {
          text: text,
          files: this.files,
        },
        toUserId: this.encodeService.decryptData(this.toUserId),
        date: new Date(),
        time: new Date().getTime().toString()
      });
    }
  }

  async uploadFilesMessage() {
    let lengthUploader = this.fileUrls.length;
    while (lengthUploader > 0) {
      this.uploading = true;
      let i = 0;
      let formData = new FormData();
      formData.append('files', this.fileUrls[i].file);
      if (this.fileUrls[i].file.type == '') {
        this.fileErr.push(this.fileUrls[i].file.name);
        this.toastr.error(this.fileUrls[i].file.name, 'Không thể tải');
      } else {
        this.file = await this.fileDriveService.uploadFileMessage(formData);
        this.file.file.fileType = this.fileUrls[i].file.type;
        this.files.push(this.file.file);
        this.deleteFile(i);
        lengthUploader = this.fileUrls.length;
        this.uploading = false;
      }
    }
  }

  sendAndUpdateMessages(message: Message) {
    try {
      this.messageForm.disable();
      this.socketService.sendMessage(message);
      this.messages = [...this.messages, message];
      this.setDatesForMessage();
      this.messageForm.reset();
      this.messageForm.enable();
      this.setSizeTextAreaEnter();
      this.designWebService.setSizeConversation();
      this.fileUrls = [];
      this.files = [];
    } catch (error) {
      this.toastr.error('Không thể gửi!')
    }
  }

  setDatesForMessage() {
    this.daysOld = this.days;
    this.days = [...new Set([...this.days, this.datePipe.transform(new Date(), 'MM/dd/yyyy')])];
    if (this.days.length != this.daysOld.length) {
      this.dates = [... this.dates, new Date(this.days[this.days.length - 1])]
    }
  }

  resetDates() {
    this.days = [];
    this.daysOld = [];
    this.dates = [];
  }

  getDatesFromMessage() {
    this.days = [... new Set(this.days)];
    for (let i = 0; i < this.days.length; i++) {
      this.dates.push(new Date(this.days[i]));
    }
  }

  typingResponse() {
    this.socketService.typingMessageResponse().subscribe((typing: Typing) => {
      if (typing.text != '') {
        document.getElementById('typing').style.display = 'block';
        this.designWebService.setSizeConversation();
      } else {
        document.getElementById('typing').style.display = 'none';
      }
    })
  }

  typingMessage() {
    var text = document.querySelector('textarea');
    try {
      text.oninput = () => {
        var data = {
          text: text.value,
          toUserId: this.toUserId
        }
        this.socketService.typingMessage(data);
        this.messageService.updateIsViewMessage(this.toUserId, this.userId);
        this.socketService.lastMessageWatched({ fromUserId: this.toUserId, toUserId: this.userId });
      }
    } catch (error) {
    }
  }

  lastMessageWatchedResponse() {
    this.socketService.lastMessageWatchedResponse().subscribe(message => {
      if (message) {
        this.watched = this.encodeService.decryptData(message);
      }
    })
  }

  deleteMessages() {
    this.messageService.deleteMessages(this.userId, this.toUserId).subscribe(res => {
      this.response = res;
      if (this.response.success) {
        this.toastr.success(this.response.success);
        this.getMessages(this.toUserId);
        this.resetDates();
      }
    });
  }

  closeConversation() {
    this.storageService.deleteSessionUser();
    document.getElementById('conversation').style.display = 'none';
    this.messages = undefined;
    this.toUserId = undefined;
  }

  containFileImg(fileType: String) {
    if (fileType.includes('image/')) {
      return true;
    }
    return false;
  }

  viewImage(idImage) {
    return 'https://drive.google.com/uc?export=view&id=' + idImage;
  }

  downloadFile(fileId, fileName) {
    this.fileDriveService.downloadFileMessage(fileId, fileName);
  }

  setSizeTextAreaEnter() {
    var text = document.querySelector('textarea');
    text.style.height = '41px';
    text.focus();
  }

  detectFiles(event) {
    if (event.length > 0) {
      for (let i = 0; i < event.length; i++) {
        let data = {
          file: event[i],
          imgUrl: ''
        }
        let reader = new FileReader();
        if (event[i].type.includes('image/')) {
          reader.readAsDataURL(event[i]);
        }
        reader.onload = (e: any) => {
          data.imgUrl = e.target.result;
        }
        this.fileUrls.push(data);
      }
    }
    this.fileInput.nativeElement.value = '';
    this.designWebService.setSizeConversation();
  }

  deleteFile(i) {
    this.fileUrls.splice(i, 1);
  }

  onKeyFocus() {
    var dummyEl = document.querySelector('textarea');
    var isFocused = (document.activeElement == dummyEl);
    if (isFocused) {
      this.messageService.updateIsViewMessage(this.toUserId, this.userId);
      this.socketService.lastMessageWatched({ fromUserId: this.toUserId, toUserId: this.userId });
    }
  }
}
