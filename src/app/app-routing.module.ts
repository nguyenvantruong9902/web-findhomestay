import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChatComponent } from './general/body/chat/chat.component';
import { HomeComponent } from './general/body/home/home.component';
import { AdminComponent } from './admin/admin.component';
import { NotfoundComponent } from './general/body/notfound/notfound.component';
import { BodyComponent } from './general/body/body.component';
import { PostdetailComponent } from './general/body/post/postdetail/postdetail.component';
import { PersonalviewComponent } from './general/body/user/personalview/personalview.component';
import { PostshowComponent } from './general/body/post/postshow/postshow.component';
import { PersonaleditComponent } from './general/body/user/personaledit/personaledit.component';
import { AuthGuard } from './service/auth/auth-admin.guard';
import { AddpostComponent } from './general/body/post/addpost/addpost.component';

const routes: Routes = [

  {
    path: 'Admin',
    component: AdminComponent,
    // canActivate: [AuthGuard],
    children: [
      {
        path: '',
        redirectTo: 'AddPost',
        pathMatch: 'full'
      },
      {
        path: 'AddPost',
        component: AddpostComponent
      }
    ]
  },
  {
    path: '',
    redirectTo: 'index',
    pathMatch: 'full'
  },
  {
    path: 'index',
    component: BodyComponent,
    children: [
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
      },
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: 'AddPost',
        component: AddpostComponent
      },
      {
        path: 'postdetail/:ID',
        component: PostdetailComponent
      },
      {
        path: 'user',
        children: [{
          path: 'chat',
          component: ChatComponent
        }, {
          path: '',
          redirectTo: 'chat',
          pathMatch: 'full'
        }
        ],
      },
      {
        path: 'personalview',
        component: PersonalviewComponent,
        children: [
          {
            path: '',
            redirectTo: 'postshow',
            pathMatch: 'full'
          },
          {
            path: 'postshow',
            component: PostshowComponent
          },
        ]
      },
      {
        path: 'personaledit',
        component: PersonaleditComponent
      }
    ]
  }, {
    path: '**',
    component: NotfoundComponent,
    redirectTo: ''
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
